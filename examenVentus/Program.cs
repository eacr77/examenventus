﻿using System;
using System.Collections.Generic;

namespace examenVentus
{
    class Program
    {
        
        static void Main(string[] args)
        {
            //Ejercicio 1
            Console.WriteLine("Ejercicio 1.1");
            int[] X = { 100, 200, 300};
            string salida = ConcatenaArreglo(X);
            Console.WriteLine(salida);
            //Ejercicio 2
            Console.WriteLine("Ejercicio 1.3");
            string f1 = "01/01/2001";
            string f2 = "01/02/2001";
            string resultado1 = Mes(f1);
            string resultado2 = Mes(f2);
            Console.WriteLine(resultado1);
            Console.WriteLine(resultado2);
            //Ejercicio3
            Console.WriteLine("Ejercicio 2.1");
            int[] Z = { 1,2,3,10,22,33};
            int[] resultado3 = ElementosPares(Z);

            int aux1 = 0;
            while(aux1<resultado3.Length)
            {
                Console.WriteLine(resultado3[aux1]);
                aux1++;
            }
            //Ejercicio4
            Console.WriteLine("Ejercicio 2.2");
            int[] J = {9,8,1,7,4,3 };
            int[] resultado4 = OrdenaArreglo(J);

            int aux2 = 0;
            while (aux2 < resultado4.Length)
            {
                Console.WriteLine(resultado4[aux2]);
                aux2++;
            }
            Console.ReadLine();
        }
        static string ConcatenaArreglo(int [] arreglo)
        {
            if (arreglo.Length == 0)
            {
                return "";
            }
            int count=0;
            string resultado="";
            while (count < arreglo.Length)
            {
                resultado += arreglo[count].ToString()+ (count+1==arreglo.Length?"":",");
                count++;
            }
            return resultado;
        }
        static string Mes(string fechaCadena)
        {
            try
            {
                DateTime fecha = DateTime.Parse(fechaCadena);
                int mes = fecha.Month;
                switch (mes)
                {
                    case 1: return "Enero";
                    case 2: return "Febrero";
                    case 3: return "Marzo";
                    case 4: return "Abril";
                    case 5: return "Mayo";
                    case 6: return "Junio";
                    case 7: return "Julio";
                    case 8: return "Agosto";
                    case 9: return "Septiembre";
                    case 10: return "Octubre";
                    case 11: return "Noviembre";
                    case 12: return "Diciembre";
                    default: return "Formato de fecha incorrecta";
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.ToString());
                return "Error";
            }
            
        }
        static int[] ElementosPares(int[] Entrada)
        {
            if (Entrada.Length == 0)
            {
                return Entrada;
            }
            List<int> auxArray = new List<int>();
            int cont = 0;
            int aux = 0;
            while (cont < Entrada.Length)
            { 
                if (Entrada[cont] % 2 == 0)
                {
                    auxArray.Add(Entrada[cont]);
                    aux++;
                }
                cont++;
            }
            int[] Resultado = auxArray.ToArray();
            return Resultado;
        }
        static int[] OrdenaArreglo(int[] Entrada)
        {
            if (Entrada.Length == 0)
            {
                return Entrada;
            }
            //Metodo burbuja
            for (int x = 0; x < Entrada.Length; x++)
            {
                
                for (int indiceActual = 0; indiceActual < Entrada.Length - 1;
                     indiceActual++)
                {
                    int indiceSiguienteElemento = indiceActual + 1;
                
                    if (Entrada[indiceActual] > Entrada[indiceSiguienteElemento])
                    {
                        int temporal = Entrada[indiceActual];
                        Entrada[indiceActual] = Entrada[indiceSiguienteElemento];
                        Entrada[indiceSiguienteElemento] = temporal;
                    }
                }
            }
            return Entrada;
        }
    }
}
